import { Component, OnInit } from "@angular/core";

import { MyHttpGetService } from "./http-get.services";

@Component({
  selector: "my-app",
  providers: [MyHttpGetService],  
  template: `
    <ActionBar title="My App update 22" class="action-bar"></ActionBar>
    <!-- Your UI components go here -->
  `
})
export class AppComponent implements OnInit {
  public host: string;
  public userAgent: string;
  public origin: string;
  public url: string;
  public status: number;
  public statusText: string;
  public dateHeader: string;
  public typeHeader: string;
  public serverHeader: string;

  constructor(private myService: MyHttpGetService) { }

  ngOnInit() {
    console.log('onInit');
      this.extractData();
      this.extractResponseInfo();
      this.login();
  }

  extractData() {
      this.myService.getData()
          .subscribe((result) => {
            //console.log(result);
              this.onGetDataSuccess(result);
          }, (error) => {
              this.onGetDataError(error);
          });
  }

  login() {
    this.myService.login()
        .subscribe((result) => {
          console.log('hello login');
          //console.log(JSON.stringify(result));
          console.log('------- headers ---');
          //let headers = JSON.stringify(result.headers);
          
          /*let headers = result.headers;
          let sCoo = headers.get('Set-Cookie') ;
          let hConn = headers.get('Connection') ;
          console.log(sCoo);
*/
          //getMyProfile
          this.myService.getMyProfile()
          .subscribe((result) => {
            console.log('------- getMyProfile ---');
            console.log(JSON.stringify(result));
              
          }, (error) => {
              this.onGetDataError(error);
          });
        }, (error) => {

            this.onGetDataError(error);
        });
}
  extractResponseInfo() {
      this.myService.getResponseInfo()
          .subscribe(res => {
              this.status = res.status;
              this.statusText = res.statusText;
              this.typeHeader = res.headers.get("Content-Type");
              this.dateHeader = res.headers.get("Date");
              this.serverHeader = res.headers.get("Server");
          }, (error) => {
              console.log("onGetResponseInfo" + error);
          });
  }

  private onGetDataSuccess(res) {
      this.host = res.headers.Host;
      this.userAgent = res.headers["User-Agent"];
      this.origin = res.origin;
      this.url = res.url;
  }

  private onGetDataError(error: Response | any) {
      const body = error.json() || "";
      const err = body.error || JSON.stringify(body);
      console.log("onGetDataError: " + err);
  }
}