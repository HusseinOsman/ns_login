import { Injectable } from "@angular/core";
import { Http, Headers, Response } from "@angular/http";

import { Observable as RxObservable } from "rxjs/Observable";

import "rxjs/add/operator/map";
import "rxjs/add/operator/do";

@Injectable()
export class MyHttpGetService  {
    private serverUrl = "https://httpbin.org/get";

    constructor(private http: Http) { }

    getData() {
        let headers = this.createRequestHeader();
        return this.http.get(this.serverUrl, { headers: headers })
            .map(res => res.json());
    }

    login(){
        //https://nodejs.fastgrp.net:2900/mobile/login
        let headers = this.createRequestHeader();
      
       return  this.http.post(
            'https://nodejs.fastgrp.net:2900/mobile/login',
            {name:'hussein@test.com',pass:'111111'},
            {headers: headers ,withCredentials: true}
        )
        .map(res => res);
    }

    getMyProfile(){
        //let headers = this.createRequestHeader();
        let headers = new Headers();
        // set headers here e.g.
        //headers.append("AuthKey", "my-key");
        return this.http.get('https://nodejs.fastgrp.net:2900/mobile/kitchens/getKitchenByBranch', { headers: headers })
            .map(res => res.json());
    }

    getResponseInfo() {
        let headers = this.createRequestHeader();
        return this.http.get(this.serverUrl, { headers: headers })
            .do(res => res);
    }

    private createRequestHeader() {
        let headers = new Headers();
        // set headers here e.g.
        headers.append("AuthKey", "my-key");
        headers.append("AuthToken", "my-token");
        headers.append("Content-Type", "application/json");

        return headers;
    }
}